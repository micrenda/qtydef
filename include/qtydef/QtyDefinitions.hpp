#pragma once

#include <boost/units/systems/si.hpp>
#include <boost/units/systems/cgs.hpp>
#include <boost/units/systems/temperature/celsius.hpp>
#include <boost/units/systems/temperature/fahrenheit.hpp>
#include <boost/units/systems/angle/degrees.hpp>
#include <boost/units/systems/angle/gradians.hpp>
#include <boost/units/systems/angle/revolutions.hpp>
#ifdef BOOST_UNITS_ENABLE_GAUSS
#include <boost/units/systems/gauss.hpp>
#endif

namespace dfpe
{
    // SI system
    typedef boost::units::quantity<boost::units::si::absorbed_dose>                       QtySiAbsorbedDose;
    typedef boost::units::quantity<boost::units::si::acceleration>                        QtySiAcceleration;
    typedef boost::units::quantity<boost::units::si::action>                              QtySiAction;
    typedef boost::units::quantity<boost::units::si::activity>                            QtySiActivity;
    typedef boost::units::quantity<boost::units::si::amount>                              QtySiAmount;
    typedef boost::units::quantity<boost::units::si::angular_acceleration>                QtySiAngularAcceleration;
    typedef boost::units::quantity<boost::units::si::angular_momentum>                    QtySiAngularMomentum;
    typedef boost::units::quantity<boost::units::si::angular_velocity>                    QtySiAngularVelocity;
    typedef boost::units::quantity<boost::units::si::area>                                QtySiArea;
    typedef boost::units::quantity<boost::units::si::capacitance>                         QtySiCapacitance;
    typedef boost::units::quantity<boost::units::si::catalytic_activity>                  QtySiCatalyticActivity;
    typedef boost::units::quantity<boost::units::si::conductance>                         QtySiConductance;
    typedef boost::units::quantity<boost::units::si::conductivity>                        QtySiConductivity;
    typedef boost::units::quantity<boost::units::si::current>                             QtySiCurrent;
    typedef boost::units::quantity<boost::units::si::dimensionless>                       QtySiDimensionless;
    typedef boost::units::quantity<boost::units::si::dose_equivalent>                     QtySiDoseEquivalent;
    typedef boost::units::quantity<boost::units::si::dynamic_viscosity>                   QtySiDynamicViscosity;
    typedef boost::units::quantity<boost::units::si::electric_charge>                     QtySiElectricCharge;
    typedef boost::units::quantity<boost::units::si::electric_potential>                  QtySiElectricPotential;
    typedef boost::units::quantity<boost::units::si::energy>                              QtySiEnergy;
    typedef boost::units::quantity<boost::units::si::force>                               QtySiForce;
    typedef boost::units::quantity<boost::units::si::frequency>                           QtySiFrequency;
    typedef boost::units::quantity<boost::units::si::illuminance>                         QtySiIlluminance;
    typedef boost::units::quantity<boost::units::si::impedance>                           QtySiImpedance;
    typedef boost::units::quantity<boost::units::si::inductance>                          QtySiInductance;
    typedef boost::units::quantity<boost::units::si::kinematic_viscosity>                 QtySiKinematicViscosity;
    typedef boost::units::quantity<boost::units::si::length>                              QtySiLength;
    typedef boost::units::quantity<boost::units::si::luminous_flux>                       QtySiLuminousFlux;
    typedef boost::units::quantity<boost::units::si::luminous_intensity>                  QtySiLuminousIntensity;
    typedef boost::units::quantity<boost::units::si::magnetic_field_intensity>            QtySiMagneticFieldIntensity;
    typedef boost::units::quantity<boost::units::si::magnetic_flux>                       QtySiMagneticFlux;
    typedef boost::units::quantity<boost::units::si::magnetic_flux_density>               QtySiMagneticFluxDensity;
    typedef boost::units::quantity<boost::units::si::mass>                                QtySiMass;
    typedef boost::units::quantity<boost::units::si::mass_density>                        QtySiMassDensity;
    typedef boost::units::quantity<boost::units::si::moment_of_inertia>                   QtySiMomentOfInertia;
    typedef boost::units::quantity<boost::units::si::momentum>                            QtySiMomentum;
    typedef boost::units::quantity<boost::units::si::permeability>                        QtySiPermeability;
    typedef boost::units::quantity<boost::units::si::permittivity>                        QtySiPermittivity;
    typedef boost::units::quantity<boost::units::si::plane_angle>                         QtySiPlaneAngle;
    typedef boost::units::quantity<boost::units::si::power>                               QtySiPower;
    typedef boost::units::quantity<boost::units::si::pressure>                            QtySiPressure;
    typedef boost::units::quantity<boost::units::si::reluctance>                          QtySiReluctance;
    typedef boost::units::quantity<boost::units::si::resistance>                          QtySiResistance;
    typedef boost::units::quantity<boost::units::si::resistivity>                         QtySiResistivity;
    typedef boost::units::quantity<boost::units::si::solid_angle>                         QtySiSolidAngle;
    typedef boost::units::quantity<boost::units::si::surface_density>                     QtySiSurfaceDensity;
    typedef boost::units::quantity<boost::units::si::surface_tension>                     QtySiSurfaceTension;
    typedef boost::units::quantity<boost::units::si::temperature>                         QtySiTemperature;
    typedef boost::units::quantity<boost::units::si::time>                                QtySiTime;
    typedef boost::units::quantity<boost::units::si::torque>                              QtySiTorque;
    typedef boost::units::quantity<boost::units::si::velocity>                            QtySiVelocity;
    typedef boost::units::quantity<boost::units::si::volume>                              QtySiVolume;
    typedef boost::units::quantity<boost::units::si::wavenumber>                          QtySiWavenumber;

    // CGS system
    typedef boost::units::quantity<boost::units::cgs::acceleration>                       QtyCgsAcceleration;
    typedef boost::units::quantity<boost::units::cgs::area>                               QtyCgsArea;
    typedef boost::units::quantity<boost::units::cgs::current>                            QtyCgsCurrent;
    typedef boost::units::quantity<boost::units::cgs::dimensionless>                      QtyCgsDimensionless;
    typedef boost::units::quantity<boost::units::cgs::dynamic_viscosity>                  QtyCgsDynamicViscosity;
    typedef boost::units::quantity<boost::units::cgs::energy>                             QtyCgsEnergy;
    typedef boost::units::quantity<boost::units::cgs::force>                              QtyCgsForce;
    typedef boost::units::quantity<boost::units::cgs::frequency>                          QtyCgsFrequency;
    typedef boost::units::quantity<boost::units::cgs::kinematic_viscosity>                QtyCgsKinematicViscosity;
    typedef boost::units::quantity<boost::units::cgs::length>                             QtyCgsLength;
    typedef boost::units::quantity<boost::units::cgs::mass>                               QtyCgsMass;
    typedef boost::units::quantity<boost::units::cgs::mass_density>                       QtyCgsMassDensity;
    typedef boost::units::quantity<boost::units::cgs::momentum>                           QtyCgsMomentum;
    typedef boost::units::quantity<boost::units::cgs::power>                              QtyCgsPower;
    typedef boost::units::quantity<boost::units::cgs::pressure>                           QtyCgsPressure;
    typedef boost::units::quantity<boost::units::cgs::time>                               QtyCgsTime;
    typedef boost::units::quantity<boost::units::cgs::velocity>                           QtyCgsVelocity;
    typedef boost::units::quantity<boost::units::cgs::volume>                             QtyCgsVolume;
    typedef boost::units::quantity<boost::units::cgs::wavenumber>                         QtyCgsWavenumber;

    // TEMPERATURE system
    typedef boost::units::quantity<boost::units::celsius::temperature>                    QtyTemperatureCelsius;
    typedef boost::units::quantity<boost::units::fahrenheit::temperature>                 QtyTemperatureFahrenheit;

    // ANGLE system
    typedef boost::units::quantity<boost::units::degree::plane_angle>                     QtyAngleDegree;
    typedef boost::units::quantity<boost::units::gradian::plane_angle>                    QtyAngleGradian;
    typedef boost::units::quantity<boost::units::revolution::plane_angle>                 QtyAngleRevolution;

#ifdef BOOST_UNITS_ENABLE_GAUSS
    // GAUSS system
    typedef boost::units::quantity<boost::units::gauss::acceleration>                     QtyGaussAcceleration;
    typedef boost::units::quantity<boost::units::gauss::area>                             QtyGaussArea;
    typedef boost::units::quantity<boost::units::gauss::capacitance>                      QtyGaussCapacitance;
    typedef boost::units::quantity<boost::units::gauss::conductance>                      QtyGaussConductance;
    typedef boost::units::quantity<boost::units::gauss::current>                          QtyGaussCurrent;
    typedef boost::units::quantity<boost::units::gauss::dimensionless>                    QtyGaussDimensionless;
    typedef boost::units::quantity<boost::units::gauss::dynamic_viscosity>                QtyGaussDynamicViscosity;
    typedef boost::units::quantity<boost::units::gauss::electric_charge>                  QtyGaussElectricCharge;
    typedef boost::units::quantity<boost::units::gauss::electric_potential>               QtyGaussElectricPotential;
    typedef boost::units::quantity<boost::units::gauss::energy>                           QtyGaussEnergy;
    typedef boost::units::quantity<boost::units::gauss::force>                            QtyGaussForce;
    typedef boost::units::quantity<boost::units::gauss::frequency>                        QtyGaussFrequency;
    typedef boost::units::quantity<boost::units::gauss::inductance>                       QtyGaussInductance;
    typedef boost::units::quantity<boost::units::gauss::kinematic_viscosity>              QtyGaussKinematicViscosity;
    typedef boost::units::quantity<boost::units::gauss::length>                           QtyGaussLength;
    typedef boost::units::quantity<boost::units::gauss::magnetic_flux>                    QtyGaussMagneticFlux;
    typedef boost::units::quantity<boost::units::gauss::magnetic_flux_density>            QtyGaussMagneticFluxDensity;
    typedef boost::units::quantity<boost::units::gauss::mass>                             QtyGaussMass;
    typedef boost::units::quantity<boost::units::gauss::mass_density>                     QtyGaussMassDensity;
    typedef boost::units::quantity<boost::units::gauss::momentum>                         QtyGaussMomentum;
    typedef boost::units::quantity<boost::units::gauss::power>                            QtyGaussPower;
    typedef boost::units::quantity<boost::units::gauss::pressure>                         QtyGaussPressure;
    typedef boost::units::quantity<boost::units::gauss::resistance>                       QtyGaussResistance;
    typedef boost::units::quantity<boost::units::gauss::time>                             QtyGaussTime;
    typedef boost::units::quantity<boost::units::gauss::velocity>                         QtyGaussVelocity;
    typedef boost::units::quantity<boost::units::gauss::volume>                           QtyGaussVolume;
    typedef boost::units::quantity<boost::units::gauss::wavenumber>                       QtyGaussWavenumber;
#endif

}
