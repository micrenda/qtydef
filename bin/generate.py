#!/usr/bin/env python3
import argparse
import os
import sys

parser = argparse.ArgumentParser()

parser.add_argument('base_dirs',   nargs='+', help='directory where to search for boost units')

parser.add_argument('-o', '--output',    default='include/QtyDefinitions.hpp',  nargs='?', help='defination file')
parser.add_argument('-n', '--namespace', default='dfpe', help='namespace where to generate the typedefs')

args = parser.parse_args()

namespace = args.namespace

includes = []
lines = []
            
for base_dir in args.base_dirs:
    
    print('Scanning: {0}'.format(base_dir))
    
    boost_units_dir = base_dir + '/boost/units'

    if os.path.isdir(boost_units_dir) and os.path.exists(boost_units_dir):

        systems = ['si','cgs', 'gauss', 'temperature', 'angle']
        filter_out = ['io', 'base', 'codata_constants', 'prefixes']

        boost_units_system_dir = boost_units_dir + '/systems'
        
        if os.path.isdir(boost_units_system_dir) and os.path.exists(boost_units_system_dir):

            for system in systems:
                current_dir =  boost_units_system_dir + '/' + system; 
                if os.path.isdir(current_dir) and os.path.exists(current_dir):

                    if system in ('gauss') :
                        includes.append('#ifdef BOOST_UNITS_ENABLE_{}'.format(system.upper()))
                        lines.append('#ifdef BOOST_UNITS_ENABLE_{}'.format(system.upper()))
                                        
                    if system == 'temperature':
                        includes.append('#include <boost/units/systems/{0}/celsius.hpp>'.format(system))
                        includes.append('#include <boost/units/systems/{0}/fahrenheit.hpp>'.format(system))
                    elif system == 'angle':
                        includes.append('#include <boost/units/systems/{0}/degrees.hpp>'.format(system))
                        includes.append('#include <boost/units/systems/{0}/gradians.hpp>'.format(system))
                        includes.append('#include <boost/units/systems/{0}/revolutions.hpp>'.format(system))
                    else:
                        includes.append('#include <boost/units/systems/{0}.hpp>'.format(system))

                    lines.append('    // {0} system'.format(system.upper()))
                    
                    for current_file in sorted(os.listdir(current_dir)):
                        if os.path.isfile(current_dir + '/' + current_file):
                            basename, extension = os.path.splitext(current_file)
                            
                            if extension == '.hpp':
                                if basename not in filter_out:

                                    if system == 'temperature':
                                        qty_name = 'Qty' + system.capitalize() + ''.join([s.capitalize() for s in basename.split('_')])
                                        lines.append('    typedef boost::units::quantity<boost::units::{0}::{1}> '.format(basename, system).ljust(90) + qty_name + ';')
                                    elif system == 'angle':
                                        qty_name = 'Qty' + system.capitalize() + ''.join([s.capitalize() for s in basename.split('_')])
                                        lines.append('    typedef boost::units::quantity<boost::units::{0}::{1}> '.format(basename.rstrip('s'), 'plane_angle').ljust(90) + qty_name.rstrip('s') + ';')
                                    else:
                                        qty_name = 'Qty' + system.capitalize() + ''.join([s.capitalize() for s in basename.split('_')])
                                        lines.append('    typedef boost::units::quantity<boost::units::{0}::{1}> '.format(system, basename).ljust(90) + qty_name + ';')

                                          
                    if system in ('gauss'):
                        includes.append('#endif')      
                        lines.append('#endif')      
                    lines.append('')

            
                    
        else:
            print('ERROR: unable to find the boost units system dir ({0})'.format(boost_units_system_dir))
            sys.exit(1)
    else:
        print('ERROR: unable to find the boost units include dir ({0})'.format(boost_units_dir))
        sys.exit(1)
    

# writing the results   
with open(args.output,'w') as out:

    print("#pragma once", file=out)
    print(file=out)
    for include in includes:
        print(include, file=out)
    print(file=out)
    print('namespace {0}'.format(namespace), file=out)
    print('{', file=out)
    for line in lines:
        print(line, file=out)
    print('}', file=out)
    
    print("Generated typedefs")
