#!/usr/bin/env sh

mkdir -p include
./bin/generate.py /usr/include ~/research/boltz/betaboltz/external/gauss/include --namespace dfpe  --output include/qtydef/QtyDefinitions.hpp
